import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import AppNavbar from './AppNavbar';
//Import Boostrap CSS
//import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
  <App />
  </React.StrictMode>,
  document.getElementById('root')//all components will be displayed here(browser)
);


/*
//const name = 'John Smith';

const user = {
  firstName: 'Jane',
  lastName: 'Smith'
}

function formatName(user) {
  return user.firstName + ' ' + user.lastName
}

const element = <h1> Hello, {formatName(user)} </h1> 

  
ReactDOM.render(
  element,
  document.getElementById('root')
  );
*/
