// 1. Create a "User" state and an "unsetUser" function that will be 
// used in different components within the application.
// Application > src > App.js

// 2. Create a file called UserContext.js inside src folder


//import {Fragment} from 'react';//use for multiple components not to receive an error //modules
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom';//as is an alias for convenience
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar'//based on the components folder, for the arrangemet
import Error from './pages/Error';
import Products from './pages/Products'
import ProductView from './pages/ProductView'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';

function App() {


  const [user, setUser] = useState({
    //email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })
  
  //Function toclear the localstorage for logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect( () => {
    // console.log(user)
    // console.log(localStorage)

    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then( res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined") {

        setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    }, [])


  }, [])


  return (
    //fragment will serve as parent component
    //will have access to the code that was global
<UserProvider value= {{user, setUser, unsetUser}}> 
    <Router>
      <AppNavbar />
      <Container>
      <Routes>
        <Route exact path="/" element={<Home/>} />
        <Route exact path="/products" element={<Products/>} />
        <Route exact path="/products/:productId" element={<ProductView/>} />
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/logout" element={<Logout/>} />
        <Route exact path="/register" element={<Register/>} />
        <Route path="*" element={<Error />} />
      </Routes>
      </Container>
      </Router>
</UserProvider>      
    
  );
}

export default App;
