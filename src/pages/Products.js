import PropTypes from 'prop-types';
import {Fragment, useEffect, useState} from 'react'; 
import ProductCard from '../components/ProductCard';



export default function Products() {

	
	const [products, setProducts] =useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setProducts(data.map(product => {
				return (

					<ProductCard key={product._id} productProp={product} />

					)
			}))
		})
		
	}, [])

	return (
		<Fragment>
			{products}
		</Fragment>		

		)
}

//Proptypes is used to check if the data type that is entered is correct
//shape method  - expectinf the shape / format of this object
ProductCard.propTypes = {

	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

